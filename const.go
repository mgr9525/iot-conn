package iotconn

const (
	// Status is int32
	ResStatusOk       = 1
	ResStatusErr      = 2
	ResStatusAuth     = 3
	ResStatusNotFound = 4

	// Other should be more than 100...
)
